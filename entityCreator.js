import jsf from 'json-schema-faker';
import fs from 'fs';
import faker from 'faker';
import Chance from 'chance';
import moment from 'moment';

class EntityCreator {

constructor(schema_file) {
  let rawdata = fs.readFileSync(schema_file);
  this.schema = JSON.parse(rawdata);
  let chance = new Chance();
  
  jsf.option({ 
    alwaysFakeOptionals: true,
    optionalsProbability: 1,
    fixedProbabilities: true
  })
 
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
 
  let shortDateFormat = "yyyy-MM-dd'T'HH:mm'Z'";
  var shortDate = () => moment(faker.date.recent()).format(moment.HTML5_FMT.DATETIME_LOCAL) + 'Z';
  
  let randomArrayOfPhones = () => Array.from(Array( faker.random.number(6) )).map( () => faker.phone.phoneNumber()  );
  faker.phone.randomArrayOfPhones = randomArrayOfPhones;
  
  faker.address.streetArray = () => [faker.address.streetPrefix(), faker.address.streetName(), faker.address.streetAddress(), faker.address.secondaryAddress(), faker.address.streetSuffix()]
  
  jsf.extend('faker', () => faker);
  
  jsf.format('ISO-4217', () => faker.finance.currencyCode() );
  jsf.format('ISO-639', () => chance.locale({region: false}) );
  jsf.format('short-date', () => shortDate());
  jsf.format('ISO-3166', () =>  faker.address.countryCode() );
  jsf.format('Date-time', () => shortDate()); // this is a problem with case on format
  this._jsf = jsf;
}

  get() {

  return this._jsf.generate(this.schema);
}
}

export default EntityCreator;
